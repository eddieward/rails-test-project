class DogBreedFetcher
  attr_reader :breed

  def initialize(name = nil)
    @name  = breed || 'random'
    @breed = Breed.find_or_initialize_by(name: name)
  end

  def self.fetch_all
    @breeds = [{ name: 'Random', value: 'random' }]
    begin
      breeds = JSON.parse(RestClient.get('https://dog.ceo/api/breeds/list/all'))['message']
    rescue Object => e
      breeds = { 'affenpinscher' => [], 'african' => [], 'airedale' => [] }
    end
    breeds.each do |breed|
      name = breed[0].humanize
      name = 'St. Bernard' if breed[0] == 'stbernard'
      value = breed[0]
      if breed[1].empty?
        @breeds << { name: name, value: value }
      else
        breed[1].each do |br|
          @breeds << { name: "#{br.humanize} #{name}", value: "#{value}-#{br}" }
        end
      end
    end
    @breeds
  end

  def fetch
    return @breed if @breed.pic_url.present?

    @breed.pic_url = fetch_info(@breed.name)['message']
    @breed.save && @breed
  end

  def self.fetch(name = nil)
    name ||= 'random'
    DogBreedFetcher.new(name).fetch
  end

  private

  def fetch_info(name)
    { 'message' => JSON.parse(RestClient.get("https://dog.ceo/api/breed/#{name}/images/random").body)['message'] }
  rescue Object => e
    default_body
  end

  def default_body
    {
      'status'  => 'success',
      'message' => 'https://images.dog.ceo/breeds/cattledog-australian/IMG_2432.jpg'
    }
  end
end
