class BreedsController < ApplicationController
  def index
    @breed = DogBreedFetcher.fetch params[:name]
    @breeds = DogBreedFetcher.fetch_all
  end
end
